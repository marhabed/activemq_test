package com.test.activemq.org;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import com.test.activemq.org.bean.ActiveMQMessage;

public class Receiver {

	private ConnectionFactory factory = null;
	private Connection connection = null;
	private Session session = null;
	private Queue queue = null;
	private MessageConsumer consumer = null;

	public Receiver() {

	}

	public void receiveMessage() {
		try {
			factory = new ActiveMQConnectionFactory(
					ActiveMQConnection.DEFAULT_BROKER_URL);
			connection = factory.createConnection();
			
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			queue = session.createQueue("SAMPLEQUEUE");
			consumer = session.createConsumer(queue);
			consumer.setMessageListener(new MessageListener() {
			    public void onMessage(Message msg) {
			      try {
			    	  if (msg instanceof ObjectMessage) {
			              ObjectMessage objectMessage = (ObjectMessage) msg;
			              ActiveMQMessage someObject = (ActiveMQMessage)objectMessage.getObject();
			              System.out.println(someObject.getDescription());
			          }                  // print message
			      }
			      catch (JMSException e) {
			        System.err.println("Error reading message");
			      }
			    }
			  });
			connection.start();  
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		Receiver receiver = new Receiver();
		receiver.receiveMessage();
	}

}
