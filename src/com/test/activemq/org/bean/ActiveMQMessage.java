package com.test.activemq.org.bean;

import java.io.Serializable;

public class ActiveMQMessage implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -874561824893735492L;
	
	private String code;
	private String description;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
