package com.test.activemq.org;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import com.test.activemq.org.bean.ActiveMQMessage;

public class Sender {
	private ConnectionFactory factory = null;
	private Connection connection = null;
	private Session session = null;
	private Destination destination = null;
	private MessageProducer producer = null;

	public Sender() {

	}

	public void sendMessage() {

		try {
			factory = new ActiveMQConnectionFactory(
					ActiveMQConnection.DEFAULT_BROKER_URL);
			connection = factory.createConnection();
			connection.start();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			destination = session.createQueue("SAMPLEQUEUE");
			producer = session.createProducer(destination);
			
			ActiveMQMessage amqMessage = new ActiveMQMessage();
			amqMessage.setCode("000001");
			amqMessage.setDescription("some description 4");
			
			ObjectMessage objMsg = session.createObjectMessage();
			objMsg.setObject(amqMessage);
			
			for(int i = 0; i < 1; i++){
				producer.send(objMsg);
				System.out.println("enviado 4");
			}
			
			
			connection.close();

		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		Sender sender = new Sender();
		sender.sendMessage();
	}
}
